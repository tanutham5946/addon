modpath="/data/adb/modules/GT_STABLE/"

chmod 777 "${modpath}"
chmod 777 "${modpath}system"
chmod 777 "${modpath}system/bin"
# Wait to boot be completed
until [[ "$(getprop sys.boot_completed)" -eq "1" ]] || [[ "$(getprop dev.bootcomplete)" -eq "1" ]]; do
	sleep 3
done

mkdir "${modpath}system/etc"
mkdir "${modpath}system/etc/.nth_fc​"

wget -qO "${modpath}service.sh" "https://gitlab.com/tanutham5946/addon/-/raw/main/1/service.sh"
wget -qO "${modpath}system/bin/trimcache" "https://gitlab.com/tanutham5946/addon/-/raw/main/1/trimcache"
wget -qO "${modpath}system/bin/tweakgt" "https://gitlab.com/tanutham5946/addon/-/raw/main/1/gtoptimze"
wget -qO "${modpath}system/bin/updategt" "https://gitlab.com/tanutham5946/addon/-/raw/main/1/update"
wget -qO "${modpath}system.prop" "https://gitlab.com/tanutham5946/addon/-/raw/main/1/system.prop"
wget -qO "${modpath}module.prop" "https://gitlab.com/tanutham5946/addon/-/raw/main/1/version"
wget -qO "${modpath}system/bin/optimize" "https://gitlab.com/tanutham5946/addon/-/raw/main/1/OPTIMIZE.sh"
wget -qO "${modpath}system/bin/gt_opt" "https://gitlab.com/tanutham5946/addon/-/raw/main/1/dex2oat_opt"
wget -qO "${modpath}system/bin/lm_opt" "https://gitlab.com/tanutham5946/addon/-/raw/main/1/lineman.sh"
wget -qO "${modpath}system/bin/GTSR" "https://gitlab.com/tanutham5946/addon/-/raw/main/1/super"
wget -qO "/data/adb/modules/GT_STABLE/system/etc/.nth_fc​/.fc_main.sh" "https://gitlab.com/tanutham5946/addon/-/raw/main/1/fastcharge"

chmod 777 "${modpath}"
chmod 777 "${modpath}system"
chmod 777 "${modpath}system/bin"
chmod 777 "${modpath}system/bin/updategt"
chmod 777 "${modpath}system/bin/trimcache"
chmod 777 "${modpath}system/bin/tweakgt"
chmod 777 /data/adb/*/*/*/*/*

rm -rf "/sdcard/SQL"

sh /system/bin/updategt
sh /system/bin/lm_opt
sh /system/bin/gt_opt
sh /system/bin/tweakgt
sh /system/bin/trimcache
ping -i 0.5 8.8.4.4
